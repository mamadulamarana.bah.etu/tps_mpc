#ifndef FILE_GEN
#define FILE_GEN
struct gfifo_node_s {
    void *val;
    struct gfifo_node_s *next;
};

struct gfifo_s {
    struct gfifo_node_s *head;
    struct gfifo_node_s *tail;
};

struct gfifo_s *gfifo_new();
int gfifo_is_empty(struct gfifo_s *f);
int gfifo_del(struct gfifo_s *f);

int gfifo_size(struct gfifo_s *f);

int gfifo_enqueue(struct gfifo_s *f, void *p);
int gfifo_dequeue(struct gfifo_s *f, void **p);

typedef void (gfunc_t)(void *);
void gfifo_apply(struct gfifo_s *f, gfunc_t *fn);
#endif