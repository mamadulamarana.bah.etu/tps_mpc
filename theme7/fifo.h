#ifndef FILE_INT
#define FILE_INT
struct ififo_node_s {
    int val;
    struct ififo_node_s *next;
};

struct ififo_s {
    struct ififo_node_s *head;
    struct ififo_node_s *tail;
};

struct ififo_s *ififo_new();
int ififo_is_empty(struct ififo_s *f);
int ififo_enqueue(struct ififo_s *file, int val);
int ififo_dequeue(struct ififo_s *file, int *val);
int ififo_head(const struct ififo_s *file);

typedef void (func_t)(void *);
void ififo_apply(struct ififo_s *f, func_t *fn);

void ififo_del(struct ififo_s *f);
#endif