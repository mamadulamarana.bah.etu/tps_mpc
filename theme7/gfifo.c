#include "gfifo.h"
#include <stdio.h>
#include <stdlib.h>

struct gfifo_s *gfifo_new() {
    struct gfifo_s *file = malloc(sizeof(struct gfifo_s));
    if (file == NULL) {
        return NULL;
    }
    file->head = NULL;
    file->tail = NULL;
    return file;
}

int gfifo_is_empty(struct gfifo_s *f) {
    if (f->head == NULL) {
        return 1;
    }
    return 0;
}

int gfifo_enqueue(struct gfifo_s *f, void *p) {
    struct gfifo_node_s *node = malloc(sizeof(struct gfifo_node_s));
    node->val = p;
    node->next = NULL;

    if (node == NULL) {
        return 1;
    }

    if (f->head == NULL) {
        f->head = node;
        f->tail = node;
    } else {
        f->tail->next = node;
        f->tail = node;
    }

    return 0;
}

int gfifo_dequeue(struct gfifo_s *file, void **p) {
    if (file == NULL || gfifo_is_empty(file)) {
        return 1;
    }
    struct gfifo_node_s *node = file->head;
    *p = node->val;
    file->head = file->head->next;

    if (file->head == NULL) {
        file->tail = NULL;
    }
    free(node);
    
    return 0;
}

int gfifo_del(struct gfifo_s *f) {
    if (! gfifo_is_empty(f)) {
        struct gfifo_node_s *node = f->head;
        while (node) {
            struct gfifo_node_s *removed = node;
            node = node->next;
            free(removed);
        }
        f->head = NULL;
        f->tail = NULL;
        free(f);
        return 0;
    }
    return 1;
}

int gfifo_size(struct gfifo_s *f) {
    int size = 0;
    struct gfifo_node_s *node = f->head;
    while (node) {
        size++;
        node = node->next;
    }
    return size;
}

typedef void (gfunc_t)(void *);
void gfifo_apply(struct gfifo_s *f, gfunc_t *fn) {
    if(! gfifo_is_empty(f) ) {
        struct gfifo_node_s *node = f->head;
        while (node) {
            (*fn)(node->val);
            node = node->next;
        }
    }
}

void print_value(void *v)
{
    int* n = (int *) v; 
    printf(" <- %d", *n);
}

int main(void)
{
    struct gfifo_s *gfifo;

    gfifo = gfifo_new();
    int x = 12;
    int y = 13;
    void *z;

    gfifo_enqueue(gfifo, &x); /* <- 12 */
    gfifo_enqueue(gfifo, &y); /* <- 13 <- 12 */

    printf("Valeurs enfilé : ");
    gfifo_apply(gfifo, print_value);
    gfifo_dequeue(gfifo, &z); /* 12 &  <- 13 */
    putchar('\n');
    printf("Valeur defilé : ");
    print_value(z);
    putchar('\n');
    printf("Valeurs restantes: ");
    gfifo_apply(gfifo, print_value);

    printf("\nSize : %d", gfifo_size(gfifo));

    gfifo_del(gfifo);
}
