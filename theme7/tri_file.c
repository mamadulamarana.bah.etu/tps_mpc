#include <stdio.h>
#include <stdlib.h>
#include "fifo.h"
#include "fifo.h"
#include "tri_file.h"

/* fusion de deux files d'entiers triées.
   condition d'utilisation : f1 et f2 triées
   effet de bord : f1 et f2 vides
   résultat : une nouvelle file d'entiers triée */
struct ififo_s *ififo_merge(struct ififo_s *f1, struct ififo_s *f2) {
    struct ififo_s *result = ififo_new();

    int a, b;

    while (!ififo_is_empty(f1) && !ififo_is_empty(f2)) {
        a = ififo_head(f1);
        b = ififo_head(f2);

        if (a <= b) {
            ififo_dequeue(f1, &a);
            ififo_enqueue(result, a);
        }else {
            ififo_dequeue(f2, &b);
            ififo_enqueue(result, b);
        }
    }

    while (!ififo_is_empty(f1)) {
        ififo_dequeue(f1, &a);
        ififo_enqueue(result, a);
    }

    while (!ififo_is_empty(f2)) {
        ififo_dequeue(f2, &b);
        ififo_enqueue(result, b);
    }

    ififo_del(f1);
    ififo_del(f2);

    return result;
}

/* Returns sorted queue */
struct ififo_s *sorting(int array[], int size) {

    struct ififo_s *result;

    struct ififo_s *file;

    struct ififo_s *first_file;
    struct ififo_s *second_file;

    struct gfifo_s *gfile = gfifo_new();

    for (int i = 0; i < size; i++) {
        file = ififo_new();
        ififo_enqueue(file, array[i]);
        gfifo_enqueue(gfile, file);
    }

    while (gfifo_size(gfile) >= 2) {
        gfifo_dequeue(gfile, (void **)&first_file);
        gfifo_dequeue(gfile, (void **)&second_file);
        gfifo_enqueue(gfile, ififo_merge(first_file, second_file));
    }

    gfifo_dequeue(gfile, (void **)&result);

    gfifo_del(gfile);

    return result;
}

int main() {
    int array[SIZE] = {5, 3, 1, 0, 2, 6};

    printf("Tableau non Triée : ");
    for (int i = 0; i < SIZE; i++) {
        printf(" %d ", array[i]);
    }
    printf("\n");

    struct ififo_s *sorted = sorting(array, SIZE);

    struct ififo_node_s *node = sorted->head;

    printf("Valeurs Triées: ");
    while (node){
        printf(" %d ", node->val);
        node = node->next;
    }

    ififo_del(sorted);
    return 0;
}
