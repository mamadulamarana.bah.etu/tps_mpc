#include <stdio.h>
#include <stdlib.h>
#include "fifo.h"


struct ififo_s *ififo_new() {
    struct ififo_s *file = malloc(sizeof(struct ififo_s));
    if (file == NULL) {
        return NULL;
    }
    file->head = NULL;
    file->tail = NULL;
    return file;
}

int ififo_is_empty(struct ififo_s *f) {
    if (f->head == NULL) {
        return 1;
    }
    return 0;
}

int ififo_enqueue(struct ififo_s *file, int val) {
    struct ififo_node_s *noeud = malloc(sizeof(struct ififo_node_s));
    if (noeud == NULL || file == NULL) {
        return 1;
    }
    noeud->val = val;
    noeud->next = NULL;
    if (ififo_is_empty(file)) {
        file->head = noeud;
        file->tail = noeud;
    }
    else {
        file->tail->next = noeud;
        file->tail = noeud;
    }
    return 0;
}

int ififo_dequeue(struct ififo_s *file, int *val) {
    if (file == NULL || ififo_is_empty(file)) {
        return 1;
    }
    struct ififo_node_s *node = file->head;
    *val = node->val;
    file->head = file->head->next;

    if (file->head == NULL) {
        file->tail = NULL;
    }
    free(node);
    
    return 0;
}

int ififo_head(const struct ififo_s *file) {
    int val = file->head->val;
    return val;
}

void ififo_apply(struct ififo_s *f, func_t *fn) {
    if(! ififo_is_empty(f) ) {
        struct ififo_node_s *node = f->head;
        while (node) {
            (*fn)(node->val);
            node = node->next;
        }
    }
}

void ififo_del(struct ififo_s *f) {
    if (! ififo_is_empty(f)) {
        struct ififo_node_s *node = f->head;
        while (node) {
            struct ififo_node_s *removed = node;
            node = node->next;
            free(removed);
        }
        f->head = NULL;
        f->tail = NULL;
        free(f);
    }
}

void
print_int(int i)
{
    printf("%d←", i);
}

void
main()
{
    struct ififo_s *fifo;
    int i;

    fifo = ififo_new();

    ififo_enqueue(fifo, 12);   /* → 12 → */
    ififo_enqueue(fifo, 13);   /* → 13 → 12 → */

    ififo_apply(fifo, print_int); putchar('\n');

    ififo_enqueue(fifo, 14);   /* → 14 → 13 → 12 → */
    ififo_dequeue(fifo, &i);   /* 12 & → 14 → 13 → */

    printf("%d \n", i);
    ififo_apply(fifo, print_int); putchar('\n');

    ififo_dequeue(fifo, &i);   /* 13 & → 14 → */
    ififo_dequeue(fifo, &i);   /* 14 & → → */
    ififo_apply(fifo, print_int); putchar('\n');

    ififo_del(fifo);
}

