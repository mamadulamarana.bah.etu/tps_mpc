#include <stdio.h>

extern char **environ;

/**
 * Autour des variables d'environnement
*/
int nvar() {
    int nb = 0;
    while( environ[nb] != (char *) 0) {
        nb++;
    }
    return nb;
}

int main_nvar(void) {
    int res = nvar();
    printf("le nombre de variables d'environnements : %d\n", res);
}

void mprintenv_1(){
	char** env = environ;
	while (*env != NULL) {
		printf("%s\n", *env);
		env++;
	}
}


void mprintenv(int argc, char *argv[]) {
    if (argc == 1) {
        /**Si aucun argument n'a été fourni, affiche toutes les variables d'environnement*/
        mprintenv_1();
    }
    else {
        int i = 1;
        for (i = 1; i < argc; i++) {
            char *name = argv[i];
            char **env = environ;
            while (*env != NULL) {
                int j = 0;
                while (name[j] == (*env)[j] && name[j] != '=' && (*env)[j] != '\0') {
                    j++;
                }
                if (name[j] == '\0' && (*env)[j] == '=') {
                    printf("%s\n", *env + j + 1);
                    break;
                }
                env++;
            }
        }
    }
}

int main(int argc, char *argv[]) {
    mprintenv(argc, argv);
    return 0;
}

