#include <stdio.h>
#define SIZE 10

int filter_int(const int source[], int destination[], unsigned int size, int (*f)(int)) {
    int count = 0;
    unsigned int i = 0;
    for (i = 0; i < size; i++) {
        if ((*f)(source[i]) != 0) {
            destination[count++] = source[i];
        }
    }
    return count;
}

int is_even(int n) {
    return (n % 2 == 0);
}

int main() {
    int a[SIZE] = {2, 3, 6, 5, 8, 9, 10, 11, 13, 14};

    int b[SIZE];
    int i = 0;

    printf("tableau d'origine:");
    for(i=0;i<SIZE;i++){
	    printf("%d ",a[i]);
    }

    putchar('\n');
    int count = filter_int(a,b, SIZE, is_even);

    printf("tableau d'entiers pairs ：");
    for (i = 0; i < count; i++) {
        printf("%d ", b[i]);
    }
    printf("\n");

    return 0;
}
