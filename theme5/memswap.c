#include <stdlib.h>             /* pour rand() */
#include <string.h>             /* pour memcmp() */
#include <assert.h>             /* pour assert() */
#include <stdio.h>
#include "mmemcpy.h"

/**
 * sans utilisation de mmemcpy.h
*/
void memswap(void *ptr1 , void *ptr2 , int size){
	unsigned char *newptr1=(unsigned char *)ptr1;
	unsigned char *newptr2=(unsigned char *)ptr2;

	unsigned char tmp;
    int i = 0;
	for(i=0;i<size;i++){
        tmp=newptr1[i];
        newptr1[i]=newptr2[i];
        newptr2[i]=tmp;
    }
}

void test_memswap(){
    int a=10;
    int b =20;

    printf("avant : a = %d, b = %d\n", a, b);
    memswap(&a, &b, sizeof(int));
    printf("après : a = %d, b = %d\n", a, b);

    assert(a==20);
    assert(b==10);
}



int main(){
    test_memswap();
    return 0 ;
}
