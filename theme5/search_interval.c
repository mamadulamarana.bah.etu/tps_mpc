#include <stdio.h>

/**
 * Nouvelle recherche dichotomique
*/
float * search_interval(float v, float *tab, float *end){
    if (tab > end){
        return NULL;
    }
    else{
        float *mid = tab + (end - tab)/2;
        if (*mid == v){
            return mid;
        } 
        else if (*mid < v){
            return search_interval(v, mid + 1, end);
        }
        else{
            return search_interval(v, tab, mid - 1);
        }
    } 
}

int main(){
    float tab[] ={1.0, 2.0, 3.0, 5.1, 6.2, 7.3, 8.0};
    int n = (sizeof(tab) / sizeof(int));
    float v = 5.1;
    float *res = search_interval(v, tab, n);

int i =0;
    for (i=0; i < n; i++){
        printf("%f", tab[i]);
    } 
    if (res != NULL){
        printf("la valeur v est a la %ld place", res - tab + 1);
    }

    else{
        printf("la valeur v n'a pas été trouvé");
    }

    putchar('\n');
    return 0;
} 
