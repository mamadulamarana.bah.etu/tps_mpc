#include <stdio.h>
#include <stdlib.h>

char *mstrdup(const char *str) {
    int i=0;
    for(i=0; str[i] != '\0'; i++) {};
    int len = i;
    char *ptr = malloc(len);
    for (i=0; i < len; i++) {
        ptr[i] = str[i];
    }
    ptr[i] = '\0';
    return ptr;
}

int main(int argc, char *argv[]) {
    int i=0;
    char *dup;
    for (i=1; i < argc; i++) {
        dup = mstrdup(argv[i]);
        printf("duplication de '%s' : '%s'\n",argv[i], dup);
    }
    free(dup);
    return 0;
}