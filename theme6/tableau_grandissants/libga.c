#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libga.h"

#define GA_INITIAL_SIZE 2

int ga_set(struct ga_s *ga, unsigned int index, int val) {
    if (ga == NULL) {
        return -1;
    }
    if (index >= ga->ga_size) {
        int i=0; 
        int tmp[GA_INITIAL_SIZE];
        for (i=0; i < ga->ga_size; i++) {
            tmp[i] = ga->ga_elements[i];
        }
        int *new_elements = malloc(sizeof(int)* (index + ga->ga_size) );

        for (i=0; i < ga->ga_size; i++){
            new_elements[i] = tmp[i];
        }
        free(ga->ga_elements);
        ga->ga_elements = new_elements;
        ga->ga_size = ga->ga_size + index;
        return 0;
    }
    ga->ga_elements[index] = val;
    return 0;
}

int ga_get(struct ga_s *ga, unsigned int index, int *val) {
    if (ga == NULL || index >= ga->ga_size) {
        return -1;
    }
    else {
        *val = ga->ga_elements[index];
        return 0;
    }
}

int ga_new(struct ga_s *ga) {
    if (ga == NULL) {
        return -1;
    }
    else {
        ga->ga_size = GA_INITIAL_SIZE;
        ga->ga_elements = malloc(sizeof(int)*GA_INITIAL_SIZE);
        return 0;
    }
}

int ga_del(struct ga_s *ga) {
    if (ga == NULL) {
        return -1;
    }
    else {
        ga->ga_size = 0;
        free(ga->ga_elements);
        return 0;
    }
}

int main() {
    struct ga_s ga;
    ga_new(&ga);

    // Ajouter des éléments au tableau
    //ga_set(&ga, 0, 42);
    ga_set(&ga, 1, 24);
    ga_set(&ga, 2, 10);
    ga_set(&ga, 3, 100);
    ga_set(&ga, 7, 10);

    // Afficher les éléments du tableau
    int i, val;
    for (i = 0; i < 4; i++) {
        if (ga_get(&ga, i, &val) == 0) {
            printf("ga[%d] = %d\n", i, val);
        }
    }

    // Libérer la mémoire allouée pour le tableau
    ga_del(&ga);

    return 0;
}