#include<stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>

/**
 * Methode utilitaires
*/
int put_digit(int digit) {
    if (digit >= 0 &&  digit < 10) {
        putchar(digit+'0');
        return 0;
    }
    return -1;
}

int putdecP(int d) {
    if ( d >= 10) {
        int r = d%10;
        int q = d/10;
        putdecP(q);
        put_digit(r);
        return 0;
    }
    else {
        return put_digit(d);
    }
}

int putdec(int d) {
    if( d == -2147483648) {
        int r = d%10;
        int q = d/10;
        r = -r;
        putdec(q);
        return put_digit(r);
    }

    else if ( d < 0) {
        d = -d;
        putchar('-');
        return putdecP(d);
    }
    else {
        return putdecP(d);
    }
}

/**
 * Échanger deux variables 
*/

/** Swap_int */
void swap_int(int *x, int *y) {
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

/** 
 * Division
*/
int division(int div, int divd, int *q, int *r) {
    *q = divd / div;
    *r = divd % div;
}

/** main pour swap_int et division */
int main_swap_int(void) {
    int x, y;
    printf("Enter Value for x \n");
    scanf("%d", &x);
    printf("Enter value for y \n");
    scanf("%d", &y);
    swap_int(&x, &y);
    printf("valeur de x : %d, valeur de y : %d", x, y);
    printf("\n");

    int div = 2;
    int divd = 4;
    int q;
    int r;
    division(div, divd, &q, &r);
    putdec(q);
    printf("\n");
    putdec(r);
    printf("\n");

    return 0;
}


/**
 * Sauter les espaces
*/

/**
 * Skip_spaces
*/
char * skip_spaces(char s[]) {
    int i = 0;
    for (i=0; s[i] != '\0'; i++ ) {
        if (s[i] != ' ') {
            return &s[i];
        }
    }
    char * ptr = &s[i];
    return ptr;
}

/** main_Skip_spaces*/
int main_skip_spaces(int argc, char *argv[])
{
    char * strip;
    int i; 
    assert(argc == 2);

    printf("argv  : %s\n", argv[1]);
    strip = skip_spaces(argv[1]);
    printf("strip : %s\n", strip);

    for (i=0 ; strip[i]; i++)
        strip[i] = toupper(strip[i]);

    printf("strip : %s\n", strip);
    printf("argv  : %s\n", argv[1]);
    
    exit(EXIT_SUCCESS);
}


/**
 * Swap_ptr
*/
void swap_ptr(int **ptr1, int **ptr2) {
    int * tmp = &(**ptr1);
    *ptr1 = &(**ptr2);
    *ptr2 = &(*tmp);
}

/** main_swap_ptr */
int main_swap_ptr() {
    int a, b;
    int *p = &a;
    int *q = &b;
    
    swap_ptr(&p, &q);
    
    if ((p == &b) && (q == &a)) {
        printf("OK ;)\n");
        exit(EXIT_SUCCESS);
    } else {
        printf("KO ;(\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * Recherche dichotomique
*/
int *chercher(int tab[], int v, int size) {
    int i =0;
    for (i=0; i<size; i++){
        if (tab[i] == v) {
            return &tab[i];
        }
    }
    return NULL;
}

float * search_dicho(float v, float *tab, int size) {
    if (size == 1 & tab[0] != v) {
        return NULL;
    }
    else if (size == 1 & tab[0] == v) {
        return &tab[0];
    }
    else {
        int m = (size / 2);
        if (v > m) {
            return search_dicho(v, tab + m + 1, size);
        }
        else if (v < m) {
            size = m - 1;
            return search_dicho(v, tab, size);
        }
        else if (tab[m] == v) {
            return &tab[m];
        }
    }
}

int main_rech_dicho(void ) {
    float tab[6] ={1, 2, 5, 7, 9, 10};
    float v1 = 5.0;
    float v2 = 1.0;
    float v3 = 7.0;
    printf("tableau :");
    for (int i=0; i < 6; i++) {
        printf("%f", tab[i]);
    }
    printf("\nvaleur recherché : %f\n", v1);
    float *res = &(*search_dicho(v1, tab, 6));
}

/**
 * Arguments de main()
*/
/* Ma commande echo */
int
main2 (int argc, char *argv[])
{
    int i; 

    for(i = 1; i < argc ; i++) {
        printf("%s ", argv[i]); 
    } 
    putchar('\n');

    exit(EXIT_SUCCESS); 
}
